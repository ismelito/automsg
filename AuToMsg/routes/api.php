<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\testController; 
use App\Http\Controllers\MessageController;
use App\Http\Controllers\TypeNetworkController;  
use App\Http\Controllers\SocialNetworkController; 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([

    'middleware' => ['api','cors'],
    'prefix' => 'automsg'

], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::get('user', [AuthController::class, 'getUser']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('register', [AuthController::class, 'register']);

    /********************************Routes MessageController**************************************/

    Route::get('message/list', [MessageController::class, 'list']);
    Route::post('message/create', [MessageController::class, 'create']);
    Route::put('message/update/{id}', [MessageController::class, 'update']);
    Route::get('message/detail/{id}', [MessageController::class, 'detail']);
    Route::delete('message/delete/{id}', [MessageController::class, 'delete']);
    Route::get('message/social-networks/list', [SocialNetworkController::class, 'list']);

     /********************************Routes TypeNetworkController**************************************/

     Route::get('message/typenetwork/list', [TypeNetworkController::class, 'list']);
});
