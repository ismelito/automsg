<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->getMethod() === "OPTIONS") {
            return response('a')
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,PATCH,OPTIONS')
                ->header('Access-Control-Max-Age', '600')
                ->header('Access-Control-Allow-Headers', 'Authorization, Content-Type');
        }


        // petite modif du cors en cas de download, car la fonction header() n'existe pas
        // elle est presente dans les reponse de type json, mais pas pour la classe BinaryFileResponse
        // donc faire un headers->set()
        // https://github.com/laravel/framework/blob/b073237d341549ada2f13e8e5b1d6d98f93428ec/src/Illuminate/Http/ResponseTrait.php#L15-L19
        // https://github.com/spatie/laravel-responsecache/issues/63

        $response = $next($request);

        $response->headers->set('Access-Control-Allow-Origin' , '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With, Application');

        return $response;
        //return $next($request);
    }
}
