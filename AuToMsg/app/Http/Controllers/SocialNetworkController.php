<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SocialNetwork;

class SocialNetworkController extends Controller
{
   /**
    * Liste des SocialNetwork
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function list( Request $request){
        
        $socialNetwork=SocialNetwork::SocialNetwork($request);
        return response()->json($socialNetwork);
    }

    
    /**
    * liste des réseaux sociaux avec leurs messages et qui a envoyé le message
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function SocialNetworks(){
        $socialNetwork = Message::ListSocialNetwork();        
      
        return response()->json(
            [
                'msg' =>'liste des reseaux sociax' ,
                'status' =>'success',
                'code' => 200,
                'socialnetwork' => $socialNetwork
            ]);
    } 
}
