<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Message;
use App\Models\TypeNetwork;
use App\Services\ServiceSocialNetwork;


class MessageController extends Controller
{
    /*
    * 
    */
    private $serviceSocialNetwork;

    /**
    * Create a new MessageController instance.
    *
    * @return void
    */
    public function __construct(ServiceSocialNetwork $serviceSocialNetwork)
    {
        $this->serviceSocialNetwork=$serviceSocialNetwork;
        $this->middleware('auth:api', ['except' => ['insertAutomatic']]);
    }

    /**
    * Liste des messages
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function list(){
        
        $message=Message::ListMessage();
        return response()->json([
            "messages"=> $message,
            'status' =>'success',
            'code' => 200
            ]);
    }

    /**
    * Création d'un message
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function create(Request $request){
        
       try{
            $message=Message::CreateMessage($request); 
           // $this->InsertSocialNetwork($message);
           if($message->id && empty($message->date_send)){
                $type_networks_id=$request->input('type_networks_id');            
                for($i=0;$i<count($type_networks_id);$i++){
                    $this->serviceSocialNetwork->insertSocialNetwork($message,$type_networks_id[$i]);
                }
                
            }        
            return response()->json(
                [
                    'msg' => 'l\'message s\'est enregistré avec succès',
                    'user' => $message,
                    'status' =>'success',
                    'code' => 200
                ]);
       }catch(Exception $e){
           return response()->json(
                [
                    'msg' => $e->getMessage(),
                    'status' =>'error',
                    'code' => 400
                ]);
       }
        
    }

    /**
    * Modification d'un message
    *
    * @return \Illuminate\Http\JsonResponse
    * @var $id
    */
    public function update(Request $request,$id){
        try{
            
           $message=Message::UpdateMessage($request,$id);
          // $this->InsertSocialNetwork($message);
            if($message->id && empty($message->date_send)){
                $type_networks_id=$request->input('type_networks_id');            
                for($i=0;$i<count($type_networks_id);$i++){
                    $this->serviceSocialNetwork->insertSocialNetwork($message,$type_networks_id[$i]);
                }
                
            }     
            return response()->json(
                [
                    'msg' => 'l\'message s\'est enregistré avec succès',
                    'user' => $message,
                    'status' =>'success',
                    'code' => 200
                ]);
       }catch(Exception $e){
           return response()->json(
                [
                    'msg' => $e->getMessage(),
                    'status' =>'error',
                    'code' => 400
                ]);
       }
    }

    /**
    * Détail d'un message
    *
    * @return \Illuminate\Http\JsonResponse
    * @var $id
    */
    public function detail($id){

        $message = Message::DetailMessage($id);
        return response()->json(
            [
                'msg' =>'Detail message' ,
                'status' =>'success',
                'code' => 200,
                'message' => $message
            ]);
    }

    /**
    * Suppression d'un message
    *
    * @return \Illuminate\Http\JsonResponse
    * @var $id
    */
    public function delete($id){

       $message = Message::DeleteMessage($id);        
       if($message){
            return response()->json(
                [
                    'msg' =>'Suppression du message effectuée' ,
                    'status' =>'success',
                    'code' => 200
                ]);
        }else{  
            return response()->json(
                [
                    'msg' =>'Problème de suppression des messages' ,
                    'status' =>'error',
                    'code' => 400
                ]);
        }         
    }
 

    /**
    * Insertion dans le tableau des réseaux sociaux
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function InsertSocialNetwork(Request $request,Message $message){
        
        if($message->id && empty($message->date_send)){
            $type_networks_id=$request->input('type_networks_id');            
            for($i=0;$i<count($type_networks_id);$i++){
                $this->serviceSocialNetwork->insertSocialNetwork($message,$type_networks_id[$i]);
            }            
        }else{
            return response()->json(
                [
                    'msg' =>' Insertion impossible dans le tableau des réseaux sociaux' ,
                    'status' =>'error',
                    'code' => 400
                ]);
        }
    }
        
        
}

