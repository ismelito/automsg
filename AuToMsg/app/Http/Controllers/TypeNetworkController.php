<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TypeNetwork;

class TypeNetworkController extends Controller
{
    /**
    * Liste des TypeNetwork
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function list(){
        
        $typeNetworks=TypeNetwork::TypeNetwork();
        return response()->json($typeNetworks);
    }
}
