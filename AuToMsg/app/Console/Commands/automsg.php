<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Services\ServiceSocialNetwork;
use App\Models\Message;

class automsg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:msg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insertion automatisée de messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         
       $serviceSocialNetwork=new ServiceSocialNetwork();        
       $messages= DB::table('messages')->where('status','en attente')->get();
       //Message
       
       foreach($messages as $message){
            $current_date=date("Y-m-d ");
            $current_time=date("H:i:s");            
            $date_send=intval(str_replace("-", "", $message->date_send));
            $current_d=intval(str_replace("-", "", $current_date));
            if($current_d >= $date_send ) { 
                $time_send=intval(str_replace(":", "", $message->time_send));
                $current_t=intval(str_replace(":", "", date("H:i:s")));                              
                 if($current_t >= $time_send){
                    $socialNetwork= DB::table('message_type_network')
                    ->where('message_type_network.message_id', $message->id)
                    ->join('type_networks', 'message_type_network.type_network_id', '=', 'type_networks.id')
                    ->select('type_networks.id')
                    ->get(); 
                    $serviceSocialNetwork->insertSocialNetwork($message,$socialNetwork[0]->id);
                    $messages = DB::table('messages')->where('id', $message->id)->update(['status' => 'envoyé']);
                 }               
            }
        }  
        
        /*$texto= "[" . date("Y-m-d H:i:s") . "]: Insertion automatisée de messages" ;
        Storage::append("archivo.txt", $texto);   */    
        
    }
}
