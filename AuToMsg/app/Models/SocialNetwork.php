<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB; 

class SocialNetwork extends Model
{
    use HasFactory;

    public function scopeSocialNetwork($query,$request ){
        $social_networks= DB:: table ('social_networks')->where('user', auth()->user()->id)->select('social_networks.*')->get();
        return $social_networks;

    }
}
