<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB; 

class TypeNetwork extends Model
{
    use HasFactory;

    public function scopeTypeNetwork(){
        $typeNetworks= DB:: table ('type_networks')->select('type_networks.*')->get();
        return $typeNetworks;

    }
}
