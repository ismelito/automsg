<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Models\TypeNetwork;
use App\Models\SocialNetwork;

class Message extends Model
{
    
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "user_id",
        "msg", 
        "status",
        "date_send",
        "time_send"
    ];
    /**
     * Get the user that owns the message.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
    * The typeNetworks .
    */
    public function typeNetworks()
    {
        return $this->belongsToMany(TypeNetwork::class);
    }

    public function scopeListMessage(){

        $userMessage = DB::table('messages')
                       ->where('messages.user_id', auth()->user()->id)
                       ->join('message_type_network', 'messages.id', '=', 'message_type_network.message_id')
                       ->join('type_networks', 'message_type_network.type_network_id', '=', 'type_networks.id')
                       ->select('messages.*','type_networks.type_network')
                       ->get();
        return $userMessage;
    }
    public function scopeCreateMessage($query,$request){
       /* Exemple pour tester l'insertion de mail quand il y a la planification d'heures*/
             // $time_send="08:59:15";
             //$date_send="2021-12-14";
             // $request->merge(['date_send' => $date_send,'time_send' => $time_send]);

             //------------ Note: il est également possible d'utiliser Posman --------------------//
             
       /* fin del exemple */
        if($request->input('date_send')==""){
            $status="envoyé";
            $request->merge(['status' => $status, 'user_id' => auth()->user()->id]);
            $validator=Validator::make($request->all(),[
                'msg' => 'required',
                'user_id' => 'required|integer',
                'type_networks_id' => 'required|array',
                'status'  => "required"
            ]);
            
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
            }else{
              $message=Message::create($validator->validate());
              $message=Message::find($message->id);
              $message->typeNetworks()->attach($request->input('type_networks_id'));
                
            }
       }else{
            $status="en attente";
            $request->merge(['status' => $status ,'user_id' => auth()->user()->id]);
            $validator=Validator::make($request->all(),[
                'msg' => 'required',
                'user_id' => 'required|integer',
                'date_send' => 'required',
                'time_send' => 'required',
                'status' => 'required',
                'type_networks_id' => 'required|array' 
            ]);        
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
            }else{
                $message=Message::create($validator->validate());
                $message->typeNetworks()->attach($request->input('type_networks_id'));
            }
       }    
       return $message;
    }

    public function scopeUpdateMessage($query,$request,$id){
       //Exemple pour tester l'insertion de mail quand il y a la planification d'heures
          //$time_send="08:59:15";
          // $date_send="2021-12-14";        
          //$request->merge(['date_send' => $date_send,'time_send' => $time_send]);

          //------------ Note: il est également possible d'utiliser Posman --------------------//

     // Note: il est également possible d'utiliser Posman 
        $requestAllTemp="";
        $status="en attente";
        $request->merge(['status' => $status ,'user_id' => auth()->user()->id]);
        $requestAllTemp=$request->all();
        
        $validator=Validator::make($request->all(),[
            'msg' => 'required',
            'user_id' => 'required|integer',
            'date_send' => 'required',
            'time_send' => 'required',
            'status' => 'required',
            'type_networks_id' => 'required|array' 
        ]); 
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }else{
            for($i=0; $i < count($requestAllTemp); $i++){
                unset($requestAllTemp["type_networks_id"]);           
            }
            $message=DB::table('messages')->where('id', $id)->update($requestAllTemp);
            $message=Message::find($id);
            $message->typeNetworks()->sync($request->input('type_networks_id'));
        }  
           
        return $message;
     }

     public function scopeDetailMessage($query,$id){
        $message= DB::table('messages')->where('id',$id)->get();
         return $message;
     }


     public function scopeDeleteMessage($query, $id){
        $message = DB::table('messages')->where('id',$id)->delete();
        $message_type_network = DB::table('message_type_network')->where('message_id',$id)->delete();
        if($message === 1 && $message_type_network === 2){
            return true;
        }else{
            return false;
        }
         
     }
     
    public function scopeSearchSocialNetwork($message_id){
            $socialNetwork= DB::table('message_type_network')
                            ->where('message_type_network.message_id', $message_id)
                            ->join('type_networks', 'message_type_network.type_network_id', '=', 'type_networks.id')
                            ->select('type_networks.id')
                            ->get();
            return $socialNetwork;
    }
    public function scopeWaitStatus(){
        $messages= DB::table('messages')
                   ->where('status','en attente')->get();
        return $messages;
    }
    public function scopeChangeStatusById($query,$message_id){

        $messages = DB::table('messages')
                    ->where('id', $message_id)
                    ->update(['status' => 'envoyé']);
        return $messages;
    }

    
}
